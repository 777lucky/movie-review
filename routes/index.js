const express = require('express')
const router = express.Router()

// View Routes
router.get('/', (req, res) => {
  res.render('index', { title: 'Home' })
})

module.exports = router
