const express = require('express')
const multer = require('multer')
const upload = multer()
const router = express.Router()

const userController = require('../controllers/user')
const isAuthenticated = require('../middlewares/authenticate')

router.post('/', userController.create)
router.post('/login', userController.login)
router.get('/', isAuthenticated, userController.getAll)
router.get('/activate', userController.activate)
router.get('/me', isAuthenticated, userController.getMyProfile)
router.get('/reset-password', userController.reset)
router.get('/:id', userController.getById)
router.put('/', isAuthenticated, upload.single('image'), userController.update)
router.delete('/me', isAuthenticated, userController.deleteMe)
router.delete('/:id', isAuthenticated, userController.delete)

module.exports = router
