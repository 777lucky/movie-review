const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const ImageKit = require('imagekit')
const fs = require('fs')

const User = require('../models/user')
const mailer = require('../helpers/mailer')

let registerHTML = fs
  .readFileSync(`${__dirname}/../mail-templates/activate-account.html`)
  .toString()

const imagekit = new ImageKit({
  publicKey: process.env.IMAGEKIT_PUBLIC_KEY,
  privateKey: process.env.IMAGEKIT_PRIVATE_KEY,
  urlEndpoint: process.env.IMAGEKIT_ENDPOINT
})

function isValidId (id) {
  const regex = /^[a-fA-F0-9]{24}$/

  return regex.test(id)
}

const { success, error } = require('../helpers/handler')

// Create / Register user
exports.create = async (req, res, next) => {
  const { name, email, password, password_confirmation, role } = req.body

  try {
    if (!password) return error(422, 'Password is required')
    // Check if password and its confirmation are the same
    if (password !== password_confirmation) return error(400, 'Password and its confirmation must be the same')

    const salt = await bcrypt.genSalt(10)
    const encrypted_password = await bcrypt.hash(password, salt)
    const user = await User.create({ name, email, encrypted_password, role })
    const token = jwt.sign({ _id: user._id, role: user.role }, process.env.JWT_SECRET_KEY, { expiresIn: '1d' })

    registerHTML = registerHTML.replace('{{ name }}', name)
    registerHTML = registerHTML.replace(
      '{{ activate_account_url }}',
      `${process.env.BASE_URL}/activate?token=${token}`
    )

    if (!process.env.NODE_ENV === 'test') {
      mailer({
        from: 'noreply@sukatodo.com',
        // to: user.email,
        text: 'Sukatodo',
        html: registerHTML
      })
    }

    success(res, 201, { ...user._doc, token, encrypted_password: undefined })
  } catch (err) {
    next(err)
  }
}

exports.getAll = async (req, res, next) => {
  try {
    if (req.user.role !== 'super') return error(401, 'You are not authorized to do this action')
    const users = await User.find().select('-encrypted_password')

    success(res, 200, users)
  } catch (err) {
    next(err)
  }
}

exports.getById = async (req, res, next) => {
  try {
    if (!req.params.id || !isValidId(req.params.id)) return error(422, 'Invalid _id supplied')

    const user = await User.findById(req.params.id).select(
      '-encrypted_password'
    )

    success(res, 200, user)
  } catch (err) {
    next(err)
  }
}

exports.getMyProfile = async (req, res, next) => {
  const user = await User.findById(req.user._id).select(
    '-encrypted_password'
  )

  success(res, 200, user)
}

exports.login = async (req, res, next) => {
  try {
    const { email, password } = req.body
    const user = await User.authenticate({ email, password })

    success(res, 200, user)
  } catch (err) {
    next(err)
  }
}

// Update
exports.update = async (req, res, next) => {
  delete req.body.is_confirmed

  try {
    let user
    if (req.file) {
      const uploaded = await imagekit.upload({
        file: req.file.buffer.toString('base64'),
        fileName: `${req.file.fieldname}-${
          req.file.originalname.split('.')[0]
        }-${Date.now()}.${req.file.mimetype.split('/')[1]}`
      })

      user = await User.findByIdAndUpdate(
        req.user._id,
        {
          image: uploaded.url
        },
        { new: true }
      ).select('-encrypted_password')
    } else {
      user = await User.findByIdAndUpdate(
        req.user._id,
        {
          $set: { ...req.body }
        },
        { new: true }
      ).select('-encrypted_password')
    }

    success(res, 200, user)
  } catch (err) {
    next(err)
  }
}

exports.activate = async (req, res, next) => {
  const { token } = req.query

  try {
    const validUser = jwt.verify(token, process.env.JWT_SECRET_KEY)
    if (!validUser._id) return error(400, 'Token is invalid')

    const user = await User
      .findByIdAndUpdate(validUser._id, { $set: { is_confirmed: true } }, { new: true })

    if (!user) return error(404, 'User not found')

    success(res, 200, user)
  } catch (err) {
    next(err)
  }
}

exports.delete = async (req, res, next) => {
  try {
    if (!req.params.id || !isValidId(req.params.id)) return error(400, 'Invalid user id')

    const user = await User.findByIdAndUpdate(req.params.id).select('-encrypted_password')
    success(res, 200, user)
  } catch (err) {
    next(err)
  }
}

exports.deleteMe = async (req, res, next) => {
  const user = await User.findByIdAndDelete(req.user._id).select('-encrypted_password')
  success(res, 200, user)
}

exports.reset = async (req, res, next) => {
  try {
    if (!req.query.email) return error(422, 'Email must be supplied')
    const user = await User.findOne({ email: req.query.email })

    if (!user) {
      req.flash('error', 'Your email is not registered in our system')
      res.redirect('back')
    }

    const token = jwt.sign({ _id: user._id }, process.env.JWT_SECRET_KEY, { expiresIn: '5m' })

    // if (!process.env.NODE_ENV === 'test') {
    await mailer({
      to: user.email,
      subject: `Hai ${user.name}!, Here is your reset token.`,
      from: 'noreply@sukatodo.com',
      text: 'here is your token',
      html: `
          Click this link to reset <br /> <a href="${process.env.BASE_URL}/confirm-reset?token=${token}">Reset my password</a>
        `
    })
    // }

    res.render('reset-redirect', {})
  } catch (err) {
    return next(err)
  }
}
