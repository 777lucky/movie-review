const mongoose = require('mongoose')
const uniqeValidator = require('mongoose-unique-validator')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')

const Schema = mongoose.Schema

const { CreateError } = require('../helpers/handler')
// const Auth = require('../events/auth')

const userSchema = new Schema(
  {
    name: {
      type: String,
      trim: true
    },
    email: {
      type: String,
      trim: true,
      unique: true,
      required: 'You must supply an email'
    },
    image: {
      type: String,
      default: null
    },
    encrypted_password: {
      type: String,
      required: 'Password is required'
    },
    is_confirmed: {
      type: Boolean,
      default: false
    },
    role: {
      type: String,
      enum: ['user', 'super'],
      default: 'user',
      required: true
    }
  },
  {
    versionKey: false,
    timestamps: true
  }
)

userSchema.plugin(uniqeValidator)

// Authenticate
userSchema.statics.authenticate = function ({ email, password }) {
  return new Promise((resolve, reject) => {
    if (!email) return reject(new CreateError(422, 'You must supply an email'))
    if (!password) return reject(new CreateError(422, 'You must supply a password'))

    this.findOne({ email })
      .then(user => {
        if (!user) return reject(new CreateError(404, 'No user found'))

        const validPassword = bcrypt.compareSync(
          password,
          user.encrypted_password
        )

        if (!validPassword) {
          // Auth.emit('unauthorized', {
          //   _id: user._id,
          //   email: user.email
          // })

          return reject(new CreateError(400, 'Email or password is wrong'))
        }

        const token = jwt.sign({ _id: user._id, role: user.role }, process.env.JWT_SECRET_KEY, { expiresIn: '1d' })
        return resolve(Object.assign({}, user._doc, { token, encrypted_password: undefined }))
      })
  })
}

module.exports = mongoose.model('User', userSchema)
