const supertest = require('supertest')
const mongoose = require('mongoose')
const fs = require('fs')
require('dotenv').config()
process.log = {}

const User = require('../models/user')
const Task = require('../models/task')

const { generateUser } = require('./fixtures/user')
const app = require('../app')
const request = supertest(app)

const user = generateUser()

async function removeAllCollections () {
  const collections = Object.keys(mongoose.connection.collections)
  for (const collectionName of collections) {
    const collection = mongoose.connection.collections[collectionName]
    await collection.deleteMany()
  }
}

async function dropAllCollections () {
  const collections = Object.keys(mongoose.connection.collections)
  for (const collectionName of collections) {
    const collection = mongoose.connection.collections[collectionName]
    try {
      await collection.drop()
    } catch (error) {
      // Sometimes this error happens, but you can safely ignore it
      if (error.message === 'ns not found') return
      // This error occurs when you use it.todo. You can
      // safely ignore this error too
      if (error.message.includes('a background operation is currently running')) return
      console.log(error.message)
    }
  }
}

beforeAll(async (done) => {
  mongoose
    .connect(process.env.DB_CONNECTION_TEST, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false
    })
    .then(() => {
      console.log('connected')
    })
    .catch(err => console.error(err))
  await removeAllCollections()
  done()
})

// Disconnect Mongoose
afterAll(async (done) => {
  await dropAllCollections()
  await mongoose.connection.close()
  done()
})

describe('User endpoint', () => {
  it('Create a new user', async done => {
    try {
      const res = await request.post('/api/v1/users')
        .send(user)

      const { status, data } = res.body

      expect(status).toBe(true)
      expect(res.statusCode).toEqual(201)
      expect(typeof data).toEqual('object')
      expect(data).toHaveProperty('token')
      expect(true).toEqual(true)
      done()
    } catch (err) {
      console.log(err)
    }
  })

  it('Should activate the corresponding user', done => {
    User.authenticate({ email: user.email, password: user.password })
      .then(user => {
        request
          .get(`/api/v1/users/activate?token=${user.token}`)
          .then(res => {
            const { status, data } = res.body
            expect(status).toBe(true)
            expect(typeof data).toEqual('object')
            done()
          })
      })
  })

  it('Login the created user', done => {
    request.post('/api/v1/users/login')
      .set('Content-Type', 'application/json')
      .send({ email: user.email, password: user.password })
      .then(res => {
        const { status, data } = res.body

        expect(status).toBe(true)
        expect(res.statusCode).toEqual(200)
        expect(typeof data).toEqual('object')
        expect(data).toHaveProperty('token')
        expect(true).toEqual(true)
        done()
      })
  })
})

describe('Task endpoint', () => {
  it('create a new task', done => {
    const task = {
      name: 'test',
      description: 'unit testing by chai and mocha',
      due_date: '2020-02-21'
    }
    User
      .authenticate({ email: user.email, password: user.password })
      .then(data => {
        request.post('/api/v1/tasks/create')
          .set('Content-Type', 'application/json')
          .set('Authorization', 'Bearer ' + data.token)
          .send(task)
          .then(res => {
            const { status, data } = res.body
            expect(status).toBe(true)
            expect(res.statusCode).toEqual(201)
            expect(typeof data).toEqual('object')
            // expect(data).toHaveProperty('token')
            expect(true).toEqual(true)
            done()
          })
      })
  })

  it('Show all task by userId', done => {
    User
      .authenticate({ email: user.email, password: user.password })
      .then(data => {
        request.get('/api/v1/tasks/show?page=1')
          .set('Authorization', 'Bearer ' + data.token)
          .then(res => {
            const { status, data } = res.body
            expect(status).toBe(true)
            expect(res.statusCode).toEqual(200)
            expect(typeof data).toEqual('object')
            // expect(data).toHaveProperty('token')
            expect(true).toEqual(true)
            done()
          })
      })
  })

  it('Show all task by userId filtered by Uncomplete', done => {
    User
      .authenticate({ email: user.email, password: user.password })
      .then(data => {
        request.get('/api/v1/tasks/show/filter?filter=incomplete&page=1')
          .set('Authorization', 'Bearer ' + data.token)
          .then(res => {
            const { status, data } = res.body
            expect(status).toBe(true)
            expect(res.statusCode).toEqual(200)
            expect(typeof data).toEqual('object')
            // expect(data).toHaveProperty('token')
            expect(true).toEqual(true)
            done()
          })
      })
  })

  it('Show all task by userId filtered by Complete', done => {
    User
      .authenticate({ email: user.email, password: user.password })
      .then(data => {
        request.get('/api/v1/tasks/show/filter?filter=complete&page=1')
          .set('Authorization', 'Bearer ' + data.token)
          .then(res => {
            const { status, data } = res.body
            expect(status).toBe(true)
            expect(res.statusCode).toEqual(200)
            expect(typeof data).toEqual('object')
            // expect(data).toHaveProperty('token')
            expect(true).toEqual(true)
            done()
          })
      })
  })

  it('Show all task by userId filtered by Unimportance', done => {
    User
      .authenticate({ email: user.email, password: user.password })
      .then(data => {
        request.get('/api/v1/tasks/show/filter?filter=unimportance&page=1')
          .set('Authorization', 'Bearer ' + data.token)
          .then(res => {
            const { status, data } = res.body
            expect(status).toBe(true)
            expect(res.statusCode).toEqual(200)
            expect(typeof data).toEqual('object')
            // expect(data).toHaveProperty('token')
            expect(true).toEqual(true)
            done()
          })
      })
  })
  it('Show all task by userId filtered by Importance', done => {
    User
      .authenticate({ email: user.email, password: user.password })
      .then(data => {
        request.get('/api/v1/tasks/show/filter?filter=importance&page=1')
          .set('Authorization', 'Bearer ' + data.token)
          .then(res => {
            const { status, data } = res.body
            expect(status).toBe(true)
            expect(res.statusCode).toEqual(200)
            expect(typeof data).toEqual('object')
            // expect(data).toHaveProperty('token')
            expect(true).toEqual(true)
            done()
          })
      })
  })

  it('update task by task id', done => {
    const updateTask = {
      name: 'test',
      description: 'unit testing',
      due_date: '2020-02-21'
    }
    User
      .authenticate({ email: user.email, password: user.password })
      .then(data => {
        request.get('/api/v1/tasks/show?page=1')
          .set('Authorization', 'Bearer ' + data.token)
          .then(res => {
            const i = Math.floor(Math.random() * (res.body.data.docs.length))
            request.put(`/api/v1/tasks/update/${res.body.data.docs[i]._id}`)
              .set('Content-Type', 'application/json')
              .set('Authorization', 'Bearer ' + data.token)
              .send(updateTask)
              .then(res => {
                const { status, data } = res.body
                expect(status).toBe(true)
                expect(res.statusCode).toEqual(200)
                expect(typeof data).toEqual('object')
                // expect(data).toHaveProperty('token')
                expect(true).toEqual(true)
                done()
              })
          })
      })
  })

  it('update task by completion given task id', done => {
    const updateCompletionTask = {
      completion: false
    }
    User
      .authenticate({ email: user.email, password: user.password })
      .then(data => {
        request.get('/api/v1/tasks/show?page=1')
          .set('Authorization', 'Bearer ' + data.token)
          .then(res => {
            const i = Math.floor(Math.random() * (res.body.data.docs.length))
            request.put(`/api/v1/tasks/update/${res.body.data.docs[i]._id}/complete`)
              .set('Content-Type', 'application/json')
              .set('Authorization', 'Bearer ' + data.token)
              .send(updateCompletionTask)
              .then(res => {
                const { status, data } = res.body
                expect(status).toBe(true)
                expect(res.statusCode).toEqual(200)
                expect(typeof data).toEqual('object')
                // expect(data).toHaveProperty('token')
                expect(true).toEqual(true)
                done()
              })
          })
      })
  })

  it('update task by importance given task id', done => {
    const updateImportanceTask = {
      completion: false
    }
    User
      .authenticate({ email: user.email, password: user.password })
      .then(data => {
        request.get('/api/v1/tasks/show?page=1')
          .set('Authorization', 'Bearer ' + data.token)
          .then(res => {
            const i = Math.floor(Math.random() * (res.body.data.docs.length))
            request.put(`/api/v1/tasks/update/${res.body.data.docs[i]._id}/importance`)
              .set('Content-Type', 'application/json')
              .set('Authorization', 'Bearer ' + data.token)
              .send(updateImportanceTask)
              .then(res => {
                const { status, data } = res.body
                expect(status).toBe(true)
                expect(res.statusCode).toEqual(200)
                expect(typeof data).toEqual('object')
                // expect(data).toHaveProperty('token')
                expect(true).toEqual(true)
                done()
              })
          })
      })
  })

  it('delete task by task id', done => {
    User
      .authenticate({ email: user.email, password: user.password })
      .then(data => {
        request.get('/api/v1/tasks/show?page=1')
          .set('Authorization', 'Bearer ' + data.token)
          .then(res => {
            const i = Math.floor(Math.random() * (res.body.data.docs.length))
            request.delete(`/api/v1/tasks/delete/${res.body.data.docs[i]._id}`)
              .set('Authorization', 'Bearer ' + data.token)
              .then(res => {
                const { status, data } = res.body
                expect(status).toBe(true)
                expect(res.statusCode).toEqual(200)
                expect(typeof data).toEqual('object')
                // expect(data).toHaveProperty('token')
                expect(true).toEqual(true)
                done()
              })
          })
      })
  })
})

describe('Negative Task endpoint test', () => {
  it('create a new task', done => {
    User
      .authenticate({ email: user.email, password: user.password })
      .then(data => {
        request.post('/api/v1/tasks/create')
          .set('Authorization', 'Bearer ' + data.token)
          .set('Content-Type', 'application/json')
          .then(res => {
            const { status } = res.body
            expect(status).toBe(false)
            expect(res.statusCode).toEqual(400)
            done()
          })
      })
  })

  it('Should not Show all task by userId', done => {
    User
      .authenticate({ email: user.email, password: user.password })
      .then(data => {
        request.get('/api/v1/tasks/show?page=1')
          .then(res => {
            const { status } = res.body
            expect(status).toBe(false)
            expect(res.statusCode).toEqual(400)
            done()
          })
      })
  })

  it('Should not Show all task by userId filtered by Uncomplete', done => {
    User
      .authenticate({ email: user.email, password: user.password })
      .then(data => {
        request.get('/api/v1/tasks/show/filter?filter=incomplete&page=0')
          .then(res => {
            const { status, data } = res.body
            expect(status).toBe(false)
            expect(res.statusCode).toEqual(400)
            done()
          })
      })
  })

  it('Should not Show all task by userId filtered by Complete', done => {
    User
      .authenticate({ email: user.email, password: user.password })
      .then(data => {
        request.get('/api/v1/tasks/show/filter?filter=complete&page=0')
          .then(res => {
            const { status, data } = res.body
            expect(status).toBe(false)
            expect(res.statusCode).toEqual(400)
            done()
          })
      })
  })

  it('Should not Show all task by userId filtered by Unimportance', done => {
    User
      .authenticate({ email: user.email, password: user.password })
      .then(data => {
        request.get('/api/v1/tasks/show/filter?filter=unimportance&page=0')
          .then(res => {
            const { status, data } = res.body
            expect(status).toBe(false)
            expect(res.statusCode).toEqual(400)
            done()
          })
      })
  })
  it('Should not Show all task by userId filtered by Importance', done => {
    User
      .authenticate({ email: user.email, password: user.password })
      .then(data => {
        request.get('/api/v1/tasks/show/filter?filter=importance&page=0')
          .then(res => {
            const { status, data } = res.body
            expect(status).toBe(false)
            expect(res.statusCode).toEqual(400)
            done()
          })
      })
  })

  it('Should not update task by task id', done => {
    const updateTask = {
      name: 'test',
      description: 'unit testing',
      due_date: '2020-02-21'
    }
    User
      .authenticate({ email: user.email, password: user.password })
      .then(data => {
        request.get('/api/v1/tasks/show?page=1')
          .set('Authorization', 'Bearer ' + data.token)
          .then(res => {
            const i = Math.floor(Math.random() * (res.body.data.docs.length))
            request.put(`/api/v1/tasks/update/${res.body.data.docs[i]}`)
              .set('Content-Type', 'application/json')
              .send(updateTask)
              .then(res => {
                const { status, data } = res.body
                expect(status).toBe(false)
                expect(res.statusCode).toEqual(400)
                done()
              })
          })
      })
  })

  it('Should not update task by completion given task id', done => {
    const updateCompletionTask = {
      completion: false
    }
    User
      .authenticate({ email: user.email, password: user.password })
      .then(data => {
        request.get('/api/v1/tasks/show?page=1')
          .set('Authorization', 'Bearer ' + data.token)
          .then(res => {
            const i = Math.floor(Math.random() * (res.body.data.docs.length))
            request.put(`/api/v1/tasks/update/${res.body.data.docs[i]}/complete`)
              .set('Content-Type', 'application/json')
              .send(updateCompletionTask)
              .then(res => {
                const { status } = res.body
                expect(status).toBe(false)
                expect(res.statusCode).toEqual(400)
                done()
              })
          })
      })
  })

  it('Should not update task by importance given task id', done => {
    const updateImportanceTask = {
      completion: false
    }
    User
      .authenticate({ email: user.email, password: user.password })
      .then(data => {
        request.get('/api/v1/tasks/show?page=1')
          .set('Authorization', 'Bearer ' + data.token)
          .then(res => {
            const i = Math.floor(Math.random() * (res.body.data.docs.length))
            request.put(`/api/v1/tasks/update/${res.body.data.docs[i]}/importance`)
              .set('Content-Type', 'application/json')
              .send(updateImportanceTask)
              .then(res => {
                const { status } = res.body
                expect(status).toBe(false)
                expect(res.statusCode).toEqual(400)
                done()
              })
          })
      })
  })

  it('Should not delete task by task id', done => {
    User
      .authenticate({ email: user.email, password: user.password })
      .then(data => {
        request.get('/api/v1/tasks/show?page=1')
          .set('Authorization', 'Bearer ' + data.token)
          .then(res => {
            const i = Math.floor(Math.random() * (res.body.data.docs.length))
            request.delete(`/api/v1/tasks/delete/${res.body.data.docs[i]}`)
              .then(res => {
                const { status } = res.body
                expect(status).toBe(false)
                expect(res.statusCode).toEqual(400)
                done()
              })
          })
      })
  })
})
