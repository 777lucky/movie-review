const supertest = require('supertest')
const mongoose = require('mongoose')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')
require('dotenv').config()
process.log = {}

const User = require('../models/user')

const { generateUser } = require('./fixtures/user')
const app = require('../app')
const request = supertest(app)

let user = generateUser()

async function removeAllCollections () {
  const collections = Object.keys(mongoose.connection.collections)
  for (const collectionName of collections) {
    const collection = mongoose.connection.collections[collectionName]
    await collection.deleteMany()
  }
}

async function dropAllCollections () {
  const collections = Object.keys(mongoose.connection.collections)
  for (const collectionName of collections) {
    const collection = mongoose.connection.collections[collectionName]
    try {
      await collection.drop()
    } catch (error) {
      // Sometimes this error happens, but you can safely ignore it
      if (error.message === 'ns not found') return
      // This error occurs when you use it.todo. You can
      // safely ignore this error too
      if (error.message.includes('a background operation is currently running')) return
      console.log(error.message)
    }
  }
}

beforeAll(async (done) => {
  mongoose
    .connect(process.env.DB_CONNECTION_TEST, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
      useFindAndModify: false
    })
    .then(() => {
      // console.log('connected')
    })
    .catch(err => console.error(err))
  await removeAllCollections()
  done()
})

// Connection should be close, otherwise jest will show a warning
// see https://github.com/facebook/jest/issues/7092#issuecomment-429049494

// Disconnect Mongoose
afterAll(async (done) => {
  await dropAllCollections()
  await mongoose.connection.close()
  done()
})

describe('User endpoint', () => {
  it('Create a new user', async done => {
    const res = await request.post('/api/v1/users')
      .send(Object.assign(user, { role: 'super' }))

    const { status, data } = res.body

    expect(status).toBe(true)
    expect(res.statusCode).toEqual(201)
    expect(typeof data).toEqual('object')
    expect(data).toHaveProperty('token')
    expect(true).toEqual(true)
    done()
  })

  it('Should return all users', done => {
    User.authenticate({ email: user.email, password: user.password })
      .then(user => {
        request
          .get('/api/v1/users')
          .set('Authorization', 'Bearer ' + user.token)
          .then(res => {
            const { status, data } = res.body

            expect(status).toBe(true)
            expect(res.statusCode).toEqual(200)
            expect(Array.isArray(data)).toEqual(true)
            expect(true).toEqual(true)
            done()
          })
      })
  })

  it('Should activate the corresponding user', done => {
    User.authenticate({ email: user.email, password: user.password })
      .then(user => {
        request
          .get(`/api/v1/users/activate?token=${user.token}`)
          .then(res => {
            const { status, data } = res.body
            expect(status).toBe(true)
            expect(typeof data).toEqual('object')
            done()
          })
      })
  })

  it('Should not activate: token invalid', done => {
    const token = jwt.sign({ noid: 'error lah' }, process.env.JWT_SECRET_KEY)
    request
      .get(`/api/v1/users/activate?token=${token}`)
      .then(res => {
        const { status, message } = res.body
        expect(status).toBe(false)
        expect(typeof message).toEqual('string')
        done()
      })
  })

  it('Should not activate: user not found', done => {
    const mail = generateUser().email
    const pass = generateUser().password
    const salt = bcrypt.genSaltSync(10)
    const encrypted_password = bcrypt.hashSync(pass, salt)
    let token = ''
    User.create({ name: user.name, email: mail, encrypted_password })
      .then(user => {
        return User.authenticate({ email: mail, password: pass })
      })
      .then(login => {
        token = login.token
        return User.findByIdAndDelete(login._id)
      })
      .then(deleted => {
        request
          .get(`/api/v1/users/activate?token=${token}`)
          .then(res => {
            const { status, message } = res.body
            expect(status).toBe(false)
            expect(typeof message).toEqual('string')
            done()
          })
      })
  })

  it('Login the created user', done => {
    request.post('/api/v1/users/login')
      .set('Content-Type', 'application/json')
      .send({ email: user.email, password: user.password })
      .then(res => {
        const { status, data } = res.body

        expect(status).toBe(true)
        expect(res.statusCode).toEqual(200)
        expect(typeof data).toEqual('object')
        expect(data).toHaveProperty('token')
        expect(true).toEqual(true)
        done()
      })
  })

  it('Should get the current user', done => {
    request.post('/api/v1/users/login')
      .set('Content-Type', 'application/json')
      .send({ email: user.email, password: user.password })
      .then(temp => {
        const token = temp.body.data.token

        request.get('/api/v1/users/me')
          .set('Authorization', 'Bearer ' + token)
          .then(res => {
            const { status, data } = res.body

            expect(status).toBe(true)
            expect(res.statusCode).toEqual(200)
            expect(typeof data).toEqual('object')
            expect(true).toEqual(true)
            done()
          })
      })
  })

  it('Should update user\'s image', done => {
    User
      .authenticate({ email: user.email, password: user.password })
      .then(data => {
        request.put('/api/v1/users')
          .set('Authorization', 'Bearer ' + data.token)
          .set('Content-Type', 'multipart/form-data')
          .attach('image', `${__dirname}/../public/images/upload-test.png`)
          .then(res => {
            expect(res.status).toBe(200)
            done()
          })
      })
  })

  it('Should update user without image', done => {
    User
      .authenticate({ email: user.email, password: user.password })
      .then(data => {
        request.put('/api/v1/users')
          .set('Authorization', 'Bearer ' + data.token)
          .send({ name: generateUser().name })
          .then(res => {
            expect(res.status).toBe(200)
            done()
          })
          .catch(err => {
            console.log(err)
          })
      })
  })

  it('Should get user by object id', done => {
    User.find()
      .then(users => {
        const i = Math.floor(Math.random() * users.length)

        request
          .get(`/api/v1/users/${users[i]._id}`)
          .then(res => {
            const { status, data } = res.body

            expect(status).toBe(true)
            expect(typeof data).toBe('object')
            done()
          })
      })
  })

  it('Should delete user by object id', done => {
    User.find()
      .then(users => {
        const i = Math.floor(Math.random() * users.length)

        User.authenticate({ email: user.email, password: user.password })
          .then(user => {
            request
              .delete(`/api/v1/users/${users[i]._id}`)
              .set('Authorization', 'Bearer ' + user.token)
              .then(res => {
                const { status, data } = res.body

                expect(status).toBe(true)
                expect(typeof data).toBe('object')
                done()
              })
          })
      })
  })
})

describe('Negative test: User', () => {
  it('Should not create user: empty password', done => {
    request.post('/api/v1/users')
      .send({ name: user.name })
      .then(res => {
        expect(res.status).toBe(422)
        expect(typeof res.body).toEqual('object')
        done()
      })
  })

  it('Should not create user: empty email', done => {
    request.post('/api/v1/users')
      .send({ name: user.name, password: user.password })
      .then(res => {
        expect(res.status).toBe(400)
        expect(typeof res.body).toEqual('object')
        done()
      })
  })

  it('Should not create user: mismatch password and confirmation', done => {
    request.post('/api/v1/users')
      .send({ name: user.name, email: generateUser().email, password: user.password, password_confirmation: 'a' })
      .then(res => {
        expect(res.status).toBe(400)
        expect(typeof res.body).toEqual('object')
        done()
      })
  })

  it('Should not login user: empty email', done => {
    request.post('/api/v1/users/login')
      .send({ password: user.password })
      .then(res => {
        expect(res.status).toBe(422)
        expect(typeof res.body).toEqual('object')
        done()
      })
  })

  it('Should not login user: empty password', done => {
    request.post('/api/v1/users/login')
      .send({ email: user.email })
      .then(res => {
        expect(res.status).toBe(422)
        expect(typeof res.body).toEqual('object')
        done()
      })
  })

  it('Should not login user: no user found', done => {
    request.post('/api/v1/users/login')
      .send({ email: generateUser().email, password: user.password })
      .then(res => {
        expect(res.status).toBe(404)
        expect(typeof res.body).toEqual('object')
        done()
      })
  })

  it('Should not login user: wrong email or password', done => {
    request.post('/api/v1/users/login')
      .send({ email: user.email, password: 'alslsdk' })
      .then(res => {
        expect(res.status).toBe(400)
        expect(typeof res.body).toEqual('object')
        done()
      })
  })

  it('Should not return user by id: incorrect id', done => {
    request.get('/api/v1/users/lds8dk')
      .then(res => {
        expect(res.status).toBe(422)
        expect(typeof res.body).toEqual('object')
        done()
      })
  })

  it('Should not delete user by id: incorrect id', done => {
    User.authenticate({ email: user.email, password: user.password })
      .then(user => {
        request
          .delete('/api/v1/users/a')
          .set('Authorization', 'Bearer ' + user.token)
          .then(res => {
            expect(res.status).toBe(400)
            expect(typeof res.body).toEqual('object')
            done()
          })
      })
  })

  it('Should reset current user\'s password', done => {
    request
      .get(`/api/v1/users/reset-password?email=${user.email}`)
      .then(res => {
        expect(res.status).toBe(200)
        expect(typeof res.text).toBe('string')
        done()
      })
  })

  it('Should not reset password: empty email', done => {
    request
      .get('/api/v1/users/reset-password?email')
      .then(res => {
        expect(res.status).toBe(422)
        done()
      })
  })

  it('Should not reset password: email not registered', done => {
    const mail = generateUser().email
    const pass = generateUser().password
    const salt = bcrypt.genSaltSync(10)
    const encrypted_password = bcrypt.hashSync(pass, salt)

    User
      .create({ name: user.name, email: mail, encrypted_password })
      .then(user => {
        return User.deleteOne({ email: mail })
      })
      .then(deleted => {
        request
          .get(`/api/v1/users/reset-password?email=${mail}`)
          .then(res => {
            expect(res.status).toBe(302)
            done()
          })
      })
  })

  it('Should delete current user', done => {
    User.authenticate({ email: user.email, password: user.password })
      .then(user => {
        request
          .delete('/api/v1/users/me')
          .set('Authorization', 'Bearer ' + user.token)
          .then(res => {
            const { status, data } = res.body

            expect(status).toBe(true)
            expect(typeof data).toBe('object')
            done()
          })
      })
  })

  it('Should not return all users', done => {
    user = generateUser()
    request.post('/api/v1/users')
      .send(user)
      .then(res => {
        User.authenticate({ email: user.email, password: user.password })
          .then(user => {
            request
              .get('/api/v1/users')
              .set('Authorization', 'Bearer ' + user.token)
              .then(res => {
                const { status, message } = res.body

                expect(status).toBe(false)
                expect(res.statusCode).toEqual(401)
                expect(typeof message).toEqual('string')
                done()
              })
          })
      })
  })

  it('Should not get user by object id', done => {
    const wrongId = Math.floor(Math.random() * 99999999999999999999)

    request
      .get(`/api/v1/users/${wrongId}`)
      .then(res => {
        const { status, message } = res.body

        expect(status).toBe(false)
        expect(typeof message).toBe('string')
        done()
      })
  })
})
