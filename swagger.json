{
  "swagger": "2.0",
  "info": {
    "description": "A minimalistic todo list application",
    "version": "1.0.0",
    "title": "Sukatodo",
    "contact": {
      "email": "hello@sukatodo.com"
    }
  },
  "schemes": [
    "http",
    "https"
  ],
  "host": "localhost:3000",
  "basePath": "/api/v1",
  "tags": [
    {
      "name": "User Endpoints",
      "description": "All endpoints related to users"
    },
    {
      "name": "Task Endpoints",
      "description": "all endpoints related to tasks"
    }
  ],
  "paths": {
    "/users": {
      "post": {
        "tags": [
          "User Endpoints"
        ],
        "summary": "Create user",
        "description": "",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "Created user object",
            "required": true,
            "schema": {
              "type": "object",
              "properties": {
                "name": {
                  "type": "string",
                  "example": "john"
                },
                "email": {
                  "type": "string",
                  "example": "john@gmail.com"
                },
                "password": {
                  "type": "string",
                  "example": "passwordsusah123"
                },
                "password_confirmation": {
                  "type": "string",
                  "example": "passwordsusah123"
                }
              }
            }
          }
        ],
        "responses": {
          "201": {
            "description": "A User object with token attached",
            "schema": {
              "$ref": "#/definitions/UserWithToken"
            }
          },
          "400": {
            "description": "Password and its confirmation must be the same"
          }
        }
      },
      "get": {
        "tags": [
          "User Endpoints"
        ],
        "summary": "Get all users",
        "description": "",
        "produces": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "Array of user object"
          }
        }
      },
      "put": {
        "tags": [
          "User Endpoints"
        ],
        "summary": "Update current user",
        "consumes": [
          "multipart/form-data",
          "application/json"
        ],
        "description": "This can only be done by the logged in user.",
        "produces": [
          "application/json"
        ],
        "security": [
          {
            "authorization": []
          }
        ],
        "parameters": [
          {
            "in": "formData",
            "name": "image",
            "type": "file",
            "description": "The file to upload."
          },
          {
            "in": "formData",
            "name": "name",
            "type": "string",
            "required": false,
            "description": "Name of the user, its not username"
          },
          {
            "in": "formData",
            "name": "email",
            "type": "string",
            "required": false,
            "description": "Email of the user."
          }
        ],
        "responses": {
          "200": {
            "description": "Successfully update user"
          },
          "404": {
            "description": "User not found"
          }
        }
      }
    },
    "/users/activate": {
      "get": {
        "tags": [
          "User Endpoints"
        ],
        "summary": "Get current user details",
        "description": "",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "query",
            "name": "token",
            "type": "string",
            "description": "Token to be use to activate the user account"
          }
        ],
        "responses": {
          "200": {
            "description": "A User object with token attached",
            "schema": {
              "$ref": "#/definitions/UserNoPassword"
            }
          },
          "400": {
            "description": "Token is invalid"
          },
          "404": {
            "description": "User not found"
          }
        }
      }
    },
    "/users/login": {
      "post": {
        "tags": [
          "User Endpoints"
        ],
        "summary": "Logs user into the system",
        "description": "",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "Created user object",
            "required": true,
            "schema": {
              "type": "object",
              "properties": {
                "email": {
                  "type": "string",
                  "example": "john@gmail.com"
                },
                "password": {
                  "type": "string",
                  "example": "passwordsusah123"
                }
              }
            }
          }
        ],
        "responses": {
          "200": {
            "description": "A User object with token attached",
            "schema": {
              "$ref": "#/definitions/UserWithToken"
            }
          },
          "404": {
            "description": "User not found"
          },
          "422": {
            "description": "You must supply an email and a password"
          }
        }
      }
    },
    "/users/me": {
      "get": {
        "tags": [
          "User Endpoints"
        ],
        "summary": "Get current user details",
        "description": "",
        "produces": [
          "application/json"
        ],
        "security": [
          {
            "authorization": []
          }
        ],
        "responses": {
          "200": {
            "description": "A User object with token attached",
            "schema": {
              "$ref": "#/definitions/UserNoPassword"
            }
          },
          "404": {
            "description": "User not found"
          },
          "422": {
            "description": "Invalid _id supplied"
          }
        }
      },
      "delete": {
        "tags": [
          "User Endpoints"
        ],
        "summary": "Delete current user",
        "description": "",
        "produces": [
          "application/json"
        ],
        "security": [
          {
            "authorization": []
          }
        ],
        "responses": {
          "200": {
            "description": "A User object with token attached",
            "schema": {
              "$ref": "#/definitions/UserNoPassword"
            }
          },
          "400": {
            "description": "Invalid _id supplied"
          },
          "404": {
            "description": "User not found"
          }
        }
      }
    },
    "/users/{_id}": {
      "get": {
        "tags": [
          "User Endpoints"
        ],
        "summary": "Get user by object id",
        "description": "",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "_id",
            "in": "path",
            "description": "Object id of the user",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "A User object with token attached",
            "schema": {
              "$ref": "#/definitions/UserNoPassword"
            }
          },
          "400": {
            "description": "Invalid _id supplied"
          },
          "404": {
            "description": "User not found"
          }
        }
      },
      "delete": {
        "tags": [
          "User Endpoints"
        ],
        "summary": "Delete user",
        "description": "This can only be done by the logged in user.",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "_id",
            "in": "path",
            "description": "The name that needs to be deleted",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Succesfully delete user"
          },
          "404": {
            "description": "User not found"
          }
        }
      }
    },
    "/users/reset-password": {
      "post": {
        "tags": [
          "User Endpoints"
        ],
        "summary": "Reset password",
        "description": "",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "Reset user password",
            "required": true,
            "schema": {
              "type": "object",
              "properties": {
                "email": {
                  "type": "string",
                  "example": "john@gmail.com"
                }
              }
            }
          }
        ],
        "responses": {
          "201": {
            "description": "A User object with token attached",
            "schema": {
              "$ref": "#/definitions/UserWithToken"
            }
          }
        }
      }
    },
    "/tasks/create": {
      "post": {
        "tags": [
          "Task Endpoints"
        ],
        "summary": "create new task",
        "description": "Ask user for task's name, task's description, and when the task will end",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/CreateTaskRequest"
            }
          }
        ],
        "responses": {
          "201": {
            "description": "Successfully create task"
          },
          "422": {
            "description": "Your input might be wrong"
          }
        },
        "security": [
          {
            "authorization": []
          }
        ]
      }
    },
    "/tasks/show": {
      "get": {
        "tags": [
          "Task Endpoints"
        ],
        "summary": "Get user's all task",
        "description": "Showing tasks by asking user for page number (only show max 10 tasks per page)",
        "parameters": [
          {
            "name": "page",
            "in": "query",
            "description": "page number",
            "required": true,
            "type": "number",
            "items": {
              "default": 1
            }
          }
        ],
        "consumes": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "Should return array of Task"
          }
        },
        "security": [
          {
            "authorization": []
          }
        ]
      }
    },
    "/tasks/show/filter": {
      "get": {
        "tags": [
          "Task Endpoints"
        ],
        "summary": "Get user's all filtered task",
        "description": "Ask user for filtering options (incomplete/ complete/ unimportance/ or importance) & page number (only show max 10 tasks per page)",
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "filter",
            "in": "query",
            "description": "filter properties",
            "required": true,
            "type": "array",
            "items": {
              "type": "string",
              "enum": [
                "incomplete",
                "complete",
                "unimportance",
                "importance"
              ],
              "default": "incomplete"
            },
            "collectionFormat": "multi"
          },
          {
            "name": "page",
            "in": "query",
            "description": "page number",
            "required": true,
            "type": "number",
            "items": {
              "default": 1
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Should return array of chosen filtered task"
          }
        },
        "security": [
          {
            "authorization": []
          }
        ]
      }
    },
    "/tasks/update/:_id": {
      "put": {
        "tags": [
          "Task Endpoints"
        ],
        "summary": "update task by taskId",
        "description": "Ask user for task's name, task's description, when the task will end, uncompleted/uncompleted, and/or importance/unimportance",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "schema": {
              "$ref": "#/definitions/UpdateTaskRequest"
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successfully update task"
          },
          "404": {
            "description": "Your input might be wrong"
          }
        },
        "security": [
          {
            "authorization": []
          }
        ]
      }
    },
    "/tasks/update/:_id/complete": {
      "put": {
        "tags": [
          "Task Endpoints"
        ],
        "summary": "update task completion by taskId",
        "description": "Ask user for completed or incompleted value",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "schema": {
              "type": "object",
              "properties": {
                "completion": {
                  "type": "boolean",
                  "example": true
                }
              }
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successfully update completion value of task"
          },
          "404": {
            "description": "Your input might be wrong"
          }
        },
        "security": [
          {
            "authorization": []
          }
        ]
      }
    },
    "/tasks/update/:_id/importance": {
      "put": {
        "tags": [
          "Task Endpoints"
        ],
        "summary": "update task importance by taskId",
        "description": "Ask user for importance or unimportance value",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "schema": {
              "type": "object",
              "properties": {
                "importance": {
                  "type": "boolean",
                  "example": true
                }
              }
            }
          }
        ],
        "responses": {
          "200": {
            "description": "Successfully update importance value of task"
          },
          "404": {
            "description": "Your input might be wrong"
          }
        },
        "security": [
          {
            "authorization": []
          }
        ]
      }
    },
    "/tasks/delete/:_id": {
      "delete": {
        "tags": [
          "Task Endpoints"
        ],
        "summary": "Delete task by taskId",
        "consumes": [
          "application/json"
        ],
        "responses": {
          "200": {
            "description": "Should delete the task"
          }
        },
        "security": [
          {
            "authorization": []
          }
        ]
      }
    }
  },
  "securityDefinitions": {
    "authorization": {
      "type": "apiKey",
      "name": "Authorization",
      "in": "header"
    }
  },
  "definitions": {
    "User": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string",
          "example": "dal"
        },
        "email": {
          "type": "string",
          "example": "dal@gmail.com"
        },
        "image": {
          "type": "string",
          "example": "http://domain.com/image.jpg"
        },
        "encrypted_password": {
          "type": "string",
          "example": "passwordsusah123"
        },
        "is_confirmed": {
          "type": "boolean",
          "example": true
        }
      },
      "xml": {
        "name": "User"
      }
    },
    "UserNoPassword": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string",
          "example": "dal"
        },
        "email": {
          "type": "string",
          "example": "dal@gmail.com"
        },
        "image": {
          "type": "string",
          "example": "http://domain.com/image.jpg"
        },
        "is_confirmed": {
          "type": "boolean",
          "example": true
        }
      },
      "xml": {
        "name": "User"
      }
    },
    "UserWithToken": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string",
          "example": "dal"
        },
        "email": {
          "type": "string",
          "example": "dal@gmail.com"
        },
        "image": {
          "type": "string",
          "example": "http://domain.com/image.jpg"
        },
        "token": {
          "type": "string",
          "example": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZTRlNjgzOWUzZjJhNmY4NTk5YTZhZTciLCJpYXQiOjE1ODIxOTY3OTN9.M1g9J-OnZ0t83xiIvlaDFYX--JwA5IFWQlNzk5wzjIY"
        },
        "is_confirmed": {
          "type": "boolean",
          "example": true
        }
      },
      "xml": {
        "name": "User"
      }
    },
    "CreateTaskRequest": {
      "type": "object",
      "required": [
        "name",
        "description",
        "due_date"
      ],
      "properties": {
        "name": {
          "type": "string",
          "example": "task's name"
        },
        "description": {
          "type": "string",
          "example": "task's description"
        },
        "due_date": {
          "type": "string",
          "example": "2020-02-21"
        }
      }
    },
    "UpdateTaskRequest": {
      "type": "object",
      "properties": {
        "name": {
          "type": "string",
          "example": "task's name"
        },
        "description": {
          "type": "string",
          "example": "task's description"
        },
        "due_date": {
          "type": "string",
          "example": "2020-02-21"
        },
        "importance": {
          "type": "boolean",
          "example": true
        },
        "completion": {
          "type": "boolean",
          "example": true
        }
      }
    },
    "ApiResponse": {
      "type": "object",
      "properties": {
        "status": {
          "type": "string"
        },
        "data": {
          "type": "string"
        }
      }
    }
  },
  "externalDocs": {
    "description": "Find out more about Sukatodo",
    "url": "http://sukatodo.com"
  }
}